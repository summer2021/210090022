## 前言

**Hi~ 这里是张俊杰（JiekerTime）**

[英文版](README_EN.md)

你可以参考项目源码于我的[本地分支](https://gitlab.summer-ospp.ac.cn/summer2021/210090022/-/tree/shardingsphere)，或者直接访问[社区仓库](https://github.com/apache/shardingsphere)。

我的所有贡献将通过我的github账户[JiekerTime](https://github.com/JiekerTime)提交，提交记录可以从仓库中查询，也可以访问我的WIKI目录中的[Development Record](https://gitlab.summer-ospp.ac.cn/summer2021/210090022/-/wikis/Development-Record)来查阅。
![](image/github.jpg)

## 关于项目
### 项目信息
项目名称：场景整合测试
#### 方案描述
##### 问题分析
我所负责的内容是项目整合测试，其根本任务完成[#9104](https://github.com/apache/shardingsphere/issues/9104)的任务和整合测试框架的搭建与完善。

但是由于是测试的原因，可能会在进行的过程中遇到SS的BUG，我要做的可能还有BUG的提出与BUG的尝试修复的过程。
##### 问题方案
解决方案按照“增量优先”的方式进行，即先尝试增加cases，然后尝试增加场景。在增量的过程中完善框架的结构与功能并修复遇到的问题。
#### 时间规划
- 7-1 —— 7-10 修复遗留问题，增加已有场景的cases
- 7-11 —— 7.30 完善并调整整合测试框架，使其支持JDBC、PROXY场景下以zookeeper为配置中心的整合测试
- 7-31 —— 8.10 丰富ZK场景下的cases，总结并修复遗留问题
- 8.11 —— 8.31 调整整合测试框架，尝试支持多种数据源
- 9.1 —— 9.15 修改cases并梳理问题修复总结
- 9.20 —— 9.30 准备结项，完善完成部分，尝试搭建影子压测的测试环境
## 附加
上述计划仅供参考，过程充满不确定性，时间规划和解决方案随时调整。SS的测试部分的开发过程包括但不局限于开源之夏的时间周期，路艰且远。

邮箱：jieker_mail@163.com