## Preface
**Hi~ This is 张俊杰（JiekerTime）**

[Chinese](README.md)

You can refer to the project source code in [my local branch](https://gitlab.summer-ospp.ac.cn/summer2021/210090022/-/tree/shardingsphere), or visit the [community repository](https://github.com/apache/shardingsphere) directly.

All my contributions will be committed through my github account [JiekerTime](https://github.com/JiekerTime). The commit record can be checked from the repository or by visiting the [Development Record](https://gitlab.summer-ospp.ac.cn/summer2021/210090022/-/wikis/Development-Record) in my WIKI directory.
![](image/github.jpg)

## About the project
### Project Information
Project Name：Scenario Integration Test
#### Program Description
##### Problem Analysis
The content I am responsible for is project integration testing, whose fundamental task is to complete the tasks of  [#9104](https://github.com/apache/shardingsphere/issues/9104) as well as the construction and improvement of the integration testing framework.

But because it is a test, it may encounter SS bugs in the process of proceeding, and what I have to do may also be the process of bug discovery and bug fixing attempts.
##### Problem programs
The solution follows an "incremental first" approach, i.e., try to add cases first, then try to add scenarios. During the incremental process, we improve the structure and functionality of the framework and fix the problems encountered.
#### Time Planning
- 7-1 —— 7-10 Fix the remaining problems and add cases of existing scenes.

- 7-11 —— 7.30 Improve and adjust the integration testing framework to support JDBC, PROXY scenarios with zookeeper as the configuration center of integration testing

- 7-31 —— 8.10 Enriched cases in ZK scenarios, summary and fix legacy issues

- 8.11 —— 8.31 Adjust the integration testing framework to try to support multiple data sources

- 9.1 —— 9.15 Modify cases and sort out problem

- 9.20 —— 9.30 Prepare to finish the project, perfect the finished part, and try to build a test environment for shadow testing
## Addition
The above plan is for reference only, the process is full of uncertainty, time planning and solutions can be adjusted at any time. the development process of the testing part of SS includes but is not limited to the time cycle of the open source summer, the road is difficult and far.

mail：jieker_mail@163.com